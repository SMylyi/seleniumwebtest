import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.Assert;
import org.testng.annotations.Test;

public class FirstTest {
    @Test
    public void checkCourseraIsOpened() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://www.coursera.org/");
        WebElement element = driver.findElement(By.xpath("//img[@alt='Coursera']"));
        boolean result = element.isDisplayed();
        System.out.println(result);
        Assert.assertTrue(result,"Coursera is opened");

        driver.quit();
    }
}
