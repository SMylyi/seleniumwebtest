package page_elements;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class MainPage {
    protected WebDriver driver;
    protected WebDriverWait wait;

    public MainPage(WebDriver driver){
        this.driver = driver;
        wait = new WebDriverWait(driver, 10);
    }

    public WebElement getSearchResults() {
        return driver.findElement(By.xpath(new XpathLocators().searchField));
    }

    public MainPage fillSearchField(String text){
        driver.findElement(By.xpath(new XpathLocators().searchField)).sendKeys(text + Keys.ENTER);
       return this;
    }

    public WebElement waitToBeClickable(By elementBy){
       return wait.until(ExpectedConditions.elementToBeClickable(elementBy));
    }
}
