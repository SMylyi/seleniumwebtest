package page_elements;
public class XpathLocators {
    public String rozetkaLogo = ("//img[@alt='Rozetka Logo']");
    public String searchField = "//input[@name='search']";
    public String appleProducts = "//h1[contains(text(),'Apple')]";
    public String iPhone = "//a[2][contains(text(),'iPhone')]";
    public String iPhone13ProMax128GB = "//*[contains(text(),' Мобильный телефон Apple iPhone 13 Pro Max 128GB Gold Официальная гарантия ')]";
    public String iPhone13ProMax128Gold = "//*[@alt='Мобильный телефон Apple iPhone 13 Pro Max 128GB Gold Официальная гарантия - изображение 1'][1]";
    public String productBuyButton = "//*/app-product-buy-btn/app-buy-button/button";
    public String appleMacBook = "//h1[contains(text(),' Apple MacBook ')]";
    public String searchButton = "//button[contains(text(),' Найти ')]";
    public String sideMenuCategoryLaptopsPc = "//*[contains(text(),'Ноут')]/parent::li[@class='menu-categories__item ng-star-inserted']";
    public String masterCardSecureImg = "//img[@alt='MasterCard Secure']";
    public String laptopsListBox = "//div/rz-catalog-settings/div/rz-sort/select";
    public String laptopOptionListBox = "//*[text()=' Новинки ']/parent::select";
    public String userIcon = "//button[@class='header__button ng-star-inserted']";
    public String email = "//input[@id='auth_email']";
    public String password = "//input[@id='auth_pass']";
    public String catalogButton = "//*[@id='fat-menu']";
    public String languageSelectionButton = "//*[@class='lang lang-header ng-star-inserted']/li";
    public String socialsList = "//div[@class='main-sidebar__block main-socials ng-star-inserted']/rz-social-icons/ul/li";
}
