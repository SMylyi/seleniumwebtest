package rozetkaTests;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.DataProvider;
import org.testng.asserts.SoftAssert;

public class BaseTests {
    protected WebDriver driver;
    protected SoftAssert softAssert;

    @BeforeSuite
    public void browserSetting() {
        WebDriverManager.chromedriver().setup();
    }

    @BeforeMethod
    public void setUpContext() {
        softAssert = new SoftAssert();
        driver = new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://rozetka.com.ua/");
    }

    @AfterMethod
    public void closeBrowser() {
        driver.quit();
    }

    @DataProvider(name = "LoginDataProvider")
    public Object[][] getData() {
        Object[][] data = {{"Login_1", "Password_1"}, {"Login_2", "Password_2"}, {"Login_3", "Password_3"}};
        return data;
    }
}
