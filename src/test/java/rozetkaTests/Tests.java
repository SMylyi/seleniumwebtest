package rozetkaTests;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.*;
import page_elements.MainPage;
import page_elements.XpathLocators;
import java.util.List;

public class Tests extends BaseTests {

    // Test 1
    @Test
    public void checkRozetkaIsOpened() {
        WebElement element = driver.findElement(By.xpath(new XpathLocators().rozetkaLogo));
        boolean result = element.isDisplayed();
        Assert.assertTrue(result, "Rozetka is opened: ");
    }

    // Test 2
    @Test
    public void findSearchFormInputOnMainRozetkaPage() {
        WebElement searchResult = new MainPage(driver).getSearchResults();
        boolean result = searchResult.isDisplayed();
        Assert.assertTrue(result, "Search-form input on main Rozetka page is found: ");
    }

    // Test 3
    @Test
    public void findTablesOfApple() {
        new MainPage(driver).fillSearchField("apple");
        WebElement appleTables = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().appleProducts));
        boolean result = appleTables.isDisplayed();
        Assert.assertTrue(result, "Apple product tables are found: ");
    }

    // Test 4
    @Test
    public void findTableOfAppleIPhone13Pro() {
        new MainPage(driver).fillSearchField("apple");
        WebElement iphone = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone));
        iphone.click();
        WebElement iPhone13Pro128GB = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone13ProMax128GB));
        boolean result = iPhone13Pro128GB.isDisplayed();
        Assert.assertTrue(result, "Table of Apple iPhone 13 pro MAX is found: ");
    }

    // Test 5
    @Test
    public void findAndClickTableOfAppleIPhone13Pro() {
        new MainPage(driver).fillSearchField("apple");
        WebElement iphone = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone));
        iphone.click();
        WebElement iPhone13Pro128GB = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone13ProMax128GB));
        iPhone13Pro128GB.click();
        WebElement iPhone13Pro128Gold = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone13ProMax128Gold));
        boolean result = iPhone13Pro128Gold.isDisplayed();
        Assert.assertTrue(result, "Table of Apple iPhone 13 pro MAX is clicked and found");
    }

    // Test 6
    @Test
    public void findAndAddToCartAppleIPhone13Pro() {
        new MainPage(driver).fillSearchField("apple");
        WebElement iphone = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone));
        iphone.click();
        WebElement iPhone13Pro128GB = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().iPhone13ProMax128GB));
        boolean result1 = iPhone13Pro128GB.isDisplayed();
        iPhone13Pro128GB.click();
        WebElement addToCart = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().productBuyButton));
        boolean result2 = addToCart.isDisplayed();
        addToCart.click();

        softAssert.assertTrue(result1, "Table of Apple iPhone 13 pro MAX is found: ");
        softAssert.assertTrue(result2, "Cart is found: ");
        softAssert.assertAll();
    }

    // Test 7
    @Test
    public void findTablesOfAppleLaptop() {
        WebElement searchResult = new MainPage(driver).getSearchResults();
        Actions builder = new Actions(driver);
        builder.clickAndHold(searchResult)
                .sendKeys("ноутбуки apple" + Keys.ENTER)
                .pause(3000)
                .build()
                .perform();
        WebElement tablesOfAppleLaptop = driver.findElement(By.xpath(new XpathLocators().appleMacBook));
        boolean result = tablesOfAppleLaptop.isDisplayed();
        Assert.assertTrue(result, "Apple laptop tables are found: ");
    }

    // Test 8
    @Test
    public void findTablesOfAppleLaptopAndClickSearchButton() {
        WebElement searchResult = new MainPage(driver).getSearchResults();
        WebElement button = driver.findElement(By.xpath(new XpathLocators().searchButton));
        Actions builder = new Actions(driver);
        builder.clickAndHold(searchResult)
                .sendKeys("ноутбуки apple")
                .pause(2000)
                .build()
                .perform();

        builder.click(button)
                .pause(2000)
                .build()
                .perform();

        WebElement tablesOfAppleLaptop = driver.findElement(By.xpath(new XpathLocators().appleMacBook));
        boolean result = tablesOfAppleLaptop.isDisplayed();
        Assert.assertTrue(result, "Apple laptop tables are found: ");
    }

    // Test 9
    @Test
    public void findInputAndRightClick() {
        WebElement searchResult = new MainPage(driver).getSearchResults();
        Actions builder = new Actions(driver);
        builder.contextClick(searchResult)
                .pause(1000)
                .build()
                .perform();
        boolean result = searchResult.isDisplayed();
        Assert.assertTrue(result, "Search-form input on main Rozetka page is found: ");
    }

    // Test 10
    @Test
    public void findMainAndScroll() {
        WebElement img = driver.findElement(By.xpath(new XpathLocators().masterCardSecureImg));
        Actions builder = new Actions(driver);
        builder.pause(2000)
                .moveToElement(img)
                .build()
                .perform();
        boolean result = img.isDisplayed();
        Assert.assertTrue(result,"Master card secure image is found: ");
    }

    // Test 11
    @Test
    public void findInputEnterClearText() {
        WebElement input = new MainPage(driver).getSearchResults();
        input.sendKeys("some text");
        boolean inputIsNotEmpty = input.getAttribute("value").isEmpty();
        input.clear();
        boolean inputIsEmpty = input.getAttribute("value").isEmpty();

        softAssert.assertFalse(inputIsNotEmpty, "Input is not empty: ");
        softAssert.assertTrue(inputIsEmpty, "Input is empty: ");
        softAssert.assertAll();
    }

    // Test 12
    @Test
    public void findLink() {
        WebElement link = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().sideMenuCategoryLaptopsPc));
        System.out.println(link.getText());
        boolean result = link.isDisplayed();
        Assert.assertTrue(result, "The link is found: ");
    }
    
    // Test 13
    @Test
    public void checkListBox() {
        new MainPage(driver).fillSearchField("ноутбуки");
        WebElement listBox = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().laptopsListBox));
        WebElement option = new MainPage(driver).waitToBeClickable(By.xpath(new XpathLocators().laptopOptionListBox));
        boolean result1 = listBox.isDisplayed();
        boolean result2 = option.isDisplayed();
        softAssert.assertTrue(result1, "The list box is found: ");
        softAssert.assertTrue(result2, "The option of list box is found: ");
        softAssert.assertAll();
    }

    // Test 14
    @Test
    public void checkRegistrationForm() {
        WebElement userIcon =  driver.findElement(By.xpath(new XpathLocators().userIcon));
        userIcon.click();

        WebElement email = driver.findElement(By.xpath(new XpathLocators().email));
        email.sendKeys("test@email.com");
        WebElement password = driver.findElement(By.xpath(new XpathLocators().password));
        password.sendKeys("password");

        boolean result1, result2, result3;
        result1 = userIcon.isDisplayed();
        result2 = email.getAttribute("value").isEmpty();
        result3 = password.getAttribute("value").isEmpty();

        softAssert.assertTrue(result1, "The user icon is found: ");
        softAssert.assertFalse(result2, "The email field is not empty: ");
        softAssert.assertFalse(result3, "The password field is not empty: ");
        softAssert.assertAll();
    }

    // Test 15
    @Test
    public void checkButtonState() {
        WebElement button = driver.findElement(By.xpath(new XpathLocators().catalogButton));
        boolean result = button.isEnabled();
        Assert.assertTrue(result, "Button enabled");
    }

    // Test 16
    @Test
    public void actionWithText() {
        WebElement searchResult = new MainPage(driver).getSearchResults();
        searchResult.sendKeys("Samsung");
        searchResult.sendKeys(Keys.CONTROL, "a");
        searchResult.sendKeys(Keys.CONTROL, "c");
        searchResult.sendKeys(Keys.CONTROL, "v");
        boolean result = searchResult.getAttribute("value").isEmpty();
        Assert.assertFalse(result,"Value is not empty: ");
    }

    // Test 17
    @Test
    public void checkScrollWithJavaScript() {
        JavascriptExecutor js = (JavascriptExecutor) driver;
        js.executeScript("window.scrollBy(0,1000)", "");
    }

    // Test 18
    @Test
    public void checkCountOfLanguageButtons() {
        List<WebElement> listOfButtons = driver.findElements(By.xpath(new XpathLocators().languageSelectionButton));
        int count = listOfButtons.size();
        boolean result = count == 2;
        Assert.assertTrue(result, "Number of language selection buttons are 2: ");
    }

    // Test 19
    @Test
    public void checkSocialsList() {
        List<WebElement> socialsList = driver.findElements(By.xpath(new XpathLocators().socialsList));
        WebElement socialIcon = socialsList.get(3);
        boolean result = socialIcon.isDisplayed();
        Assert.assertTrue(result,"Socials list is found: ");
    }

    // Test 20
    @Test(dataProvider = "LoginDataProvider")
    public void checkLoginAndPassword(String login, String password) {
        WebElement userIcon = driver.findElement(By.xpath(new XpathLocators().userIcon));
        userIcon.click();
        WebElement email = driver.findElement((By.xpath(new XpathLocators().email)));
        email.sendKeys(login);
        WebElement pswd =  driver.findElement((By.xpath(new XpathLocators().password)));
        pswd.sendKeys(password);
        boolean result1 = email.getAttribute("value").isEmpty();
        boolean result2 = pswd.getAttribute("value").isEmpty();
        softAssert.assertFalse(result1,"The email field is not empty: ");
        softAssert.assertFalse(result2, "The password field is not empty: ");
    }
}
